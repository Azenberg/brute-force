#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <openssl/sha.h>
#include <openssl/crypto.h>

// Configurations
#define MAX_LEN 100

// Global variables
pthread_t *threads;
unsigned int nbThreads;
char pass[65];

pthread_mutex_t my_mutex = PTHREAD_MUTEX_INITIALIZER;
unsigned int finded = 0;

const char *alphabet[63] =
        {
                "a", "b", "c", "d", "e", "f", "g",
                "h", "i", "j", "k", "l", "m", "n",
                "o", "p", "q", "r", "s", "t", "u",
                "v", "w", "x", "y", "z", "A", "B",
                "C", "D", "E", "F", "G", "H", "I",
                "J", "K", "L", "M", "N", "O", "P",
                "Q", "R", "S", "T", "U", "V", "W",
                "X", "Y", "Z", "1", "2", "3", "4",
                "5", "6", "7", "8", "9", "0", " "
        };


void sha256(char *string, char outputBuffer[65])
{
    unsigned char hash[SHA256_DIGEST_LENGTH];
    SHA256_CTX sha256;
    SHA256_Init(&sha256);
    SHA256_Update(&sha256, string, strlen(string));
    SHA256_Final(hash, &sha256);

    for(int i = 0; i < SHA256_DIGEST_LENGTH; i++) {
        sprintf(outputBuffer + (i * 2), "%02x", hash[i]);
    }

    outputBuffer[64] = 0;
}


void bruteImpl(int start_origin, unsigned int start, char* str, int index, int maxDepth)
{
    if (start_origin == start) {
        str[index] = *alphabet[start];

        char hashed_string[65];
        sha256(str, hashed_string);

        //if( strcmp(str, "teste") == 0 ){
        if (strcmp(hashed_string, pass) == 0) {
            printf("%s\n", str);
            pthread_mutex_lock(&my_mutex);
            finded = 1;
            pthread_mutex_unlock(&my_mutex);
            return;
        }

        if (finded == 1)
            return;

        if (index != maxDepth - 1)
            bruteImpl(-1, 0, str, index + 1, maxDepth);
    } else {
        for (int i = start; i < 63; ++i)
        {
            str[index] = *alphabet[i];

            char hashed_string[65];
            sha256(str, hashed_string);
            //if( strcmp(str, "teste") == 0 ){
            if (strcmp(hashed_string, pass) == 0) {
                printf("%s\n", str);
                pthread_mutex_lock(&my_mutex);
                finded = 1;
                pthread_mutex_unlock(&my_mutex);
                return;
            }

            if (finded == 1)
                return;

            if (index != (maxDepth - 1))
                bruteImpl(-1, 0, str, index + 1, maxDepth);
        }
    }
}

void bruteSequential(int start)
{
    int save_start = start;

    for (int i = 1; i <= MAX_LEN; i++) {
        while (start < 63) {
            char* buf = malloc(i+ 1);

            for (int j = 1; j <= i; ++j)
            {
                memset(buf, 0, i + 1);
                bruteImpl(start, start, buf, 0, j);

                if (finded == 1)
                    return;
            }

            free(buf);
            start += nbThreads;
        }

        start = save_start;
    }
}



void start_routine(int *start) {
    bruteSequential(*start);

    free(start);
}


int main(int argc, char **argv) {
    if (argc < 3) {
        printf("Error : Too few arguments given !\n");
        return 1;
    }

    if (argc > 3) {
        printf("Error : Too much arguments given !\n");
        return 1;
    }

    if (atoi(argv[1]) == 0) {
        printf("The first argument \"%s\"is not valid (not integer)\n", argv[1]);
        return 1;
    }

    nbThreads = atoi(argv[1]);
    strcpy(pass, argv[2]);

    threads = (pthread_t*) malloc(sizeof(pthread_t) * nbThreads);

    for (int i = 0; i < nbThreads; i++) {
        int *start = (int*) malloc(sizeof(int));
        *start = i;
        pthread_create(&threads[i], NULL, (void * (*) (void *)) start_routine, start);
    }

    for (unsigned int j = 0; j < nbThreads; j++) {
        pthread_join(threads[j], NULL);
    }

    free(threads);

    return 0;
}